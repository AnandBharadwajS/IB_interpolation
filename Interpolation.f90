program idw_sb
use globals
use library
implicit none

!Bounding box dimensions
integer, dimension (3) :: box_min = (/1,1,kmax/)
integer, dimension (3) :: box_max = (/imax,jmax,kmax/)

!Declaring the Variables of the Immersed Boundary Points
real, dimension(n_ib,3) :: R_ib, N
real, dimension(n_ib,4) ::  Tau
real, dimension(n_ib,n_IP) :: P_ip, T_ip, d_ip
real, dimension(n_ib,2) :: bl_ip
real, dimension(n_ib) :: P_ib, T_ib
real, dimension (n_ib,3,n_IP) :: V_ip

!Declaring the Variables of the grid
real, dimension(imax,jmax,kmax,n_vars) :: q
!real, dimension(box_max(1)-box_min(1)+1,box_max(2)-box_min(2)+1,box_max(3)-box_min(3)+1,n_vars) :: q_box
real, dimension(:,:,:,:), allocatable :: q_box

!Closest Indices to a given IB point
integer, dimension(n_ib,3) :: CI  !CI-closest indices

!Inputs
integer :: st_res, pr_int, temp_int, ss_utau, closed_profile

!Variables for Force and Moment calculations
real :: ds,Cl, Cd, vdotn, vt1, vt2, del_n, temp, time1, time2
real, dimension (3) :: F_p, F_ss, vel_ip, v_tau, F_t

integer :: i,j,k,n_ib_file,nn,i_range,j_range,k_range
i_range = box_max(1)-box_min(1)+1
j_range = box_max(2)-box_min(2)+1
k_range = box_max(3)-box_min(3)+1
allocate(q_box(i_range,j_range,k_range,n_vars))

!Reading from input file
open(10,file='Inputs')
read(10,*)
read(10,*)
read(10,*) st_res, pr_int,temp_int,st_size,incl_band,visc
read(10,*)
read(10,*)
read(10,*)
read(10,*)closed_profile,fix, xmu, iho, xalp
close(10)


!Opening 'Contour.dat' and storing the values in the array
open(10,file='contour.dat')
do i=1,n_vars+1
   read(10,*)
end do
do k=1,kmax
   do j=1,jmax
      do i=1,imax
	   read(10,*)q(i,j,k,:)
      end do
   end do
end do

q_box = q(box_min(1):box_max(1),box_min(2):box_max(2),box_min(3):box_max(3),:) !Copies data from the grid bouding box into a separate array
close(10)

!Reading 'ibsurfpts' and storing the values in an array

open(10,file='ibsurfpts')
read(10,*)n_ib_file

do i=1,n_ib
   read(10,*)R_ib(i,:),N(i,:)
end do

close(10)

!For 2D Simulations, Setting the Z-coordinates of the grid and IB to be zero
q(:,:,:,3) = 0.0
q_box(:,:,:,3) = 0.0
R_ib(:,3) = 0.0

!Searching and fixing the nearest grid cells to every IB point
print*,'Fixing Centre of Stencil...please wait'
if (st_res .eq. 0) then
        call cpu_time(time1)
	call fix_stencil(R_ib,q_box(:,:,:,1:3),CI)
        call cpu_time(time2)
        print*,time1,time2,(time2-time1)
else
	open(10,file='closest_indices.dat')
	do nn=1,n_ib
           read(10,*)CI(nn,:)
	end do
  	close(10)	
end if
print*,'Done'

!Interpolating pressure 
print*,'Pressure Interpolations...'
call idw_upwind(R_ib,q_box(:,:,:,1:3),CI,q_box(:,:,:,13),q_box(:,:,:,14),q_box(:,:,:,9),P_ib,N, &
                q_box(:,:,:,6:8),q_box(:,:,:,12))

print*,'Pressure Interpolation completed'

!Interpolations for shear stress
print*,'Shear stress calculations...'
print*,'Calculating velocities at the interpolation point...'
call idw_ip(R_ib,q_box(:,:,:,1:3),CI,q_box(:,:,:,13),q_box(:,:,:,14),q_box(:,:,:,6:7),V_ip,d_ip,N)

print*,'velocities at the IPs calculated. Now, proceeding for shear stress calculations...'
if(iho.ne.3) then
   call shear_stress(R_ib,q_box(:,:,:,1:3),CI,V_ip,d_IP,N,Tau)
   print*,'Shear stress calculations completed.'
else if(n_IP.eq.2 .and. iho.eq.3) then
   print*,'BLEDM: Boundary layer edge estimation and shear stress calculations in progress.'
   call BLEDM(R_ib,N,q_box(:,:,:,1:3),CI,q_box(:,:,:,6:7),Tau,d_IP,V_ip,q_box(:,:,:,14))
end if
print*,'Proceding for integration'
F_p(:) = 0.0
F_ss(:) = 0.0
F_t(:) = 0.0
do nn=1,n_ib !IB loop starts
   if (closed_profile .eq. 1) then
      if( nn .ne. n_ib ) then
        ds = norm2(R_ib(nn+1,:)-R_ib(nn,:))
        F_p(:) = F_p(:) - 0.5*(P_ib(nn)+P_ib(nn+1))*0.5*(N(nn,:)+N(nn+1,:))*ds 
        F_ss(:) = F_ss(:) + 0.5*(Tau(nn,4)+Tau(nn+1,4))*0.5*(Tau(nn,1:3)+Tau(nn+1,1:3))*ds
      else
        ds = norm2(R_ib(1,:)-R_ib(nn,:))
        F_p(:) = F_p(:) - 0.5*(P_ib(nn)+P_ib(1))*0.5*(N(nn,:)+N(1,:))*ds
        F_ss(:) = F_ss(:) + 0.5*(Tau(nn,4)+Tau(1,4))*0.5*(Tau(nn,1:3)+Tau(1,1:3))*ds
      end if
   else
      if( nn .ne. n_ib ) then
        ds = norm2(R_ib(nn+1,:)-R_ib(nn,:))
        F_p(:) = F_p(:) - 0.5*(P_ib(nn)+P_ib(nn+1))*0.5*(N(nn,:)+N(nn+1,:))*ds 
        F_ss(:) = F_ss(:) + 0.5*(Tau(nn,4)+Tau(nn+1,4))*0.5*(Tau(nn,1:3)+Tau(nn+1,1:3))*ds
      end if
   end if
end do !IB loop ends

open(10,file='Forces')
write(10,*)'Pressure Force (X):',F_p(1)
write(10,*)'Pressure Force (Y):',F_p(2)
write(10,*)'Shear Force(X):',F_ss(1)
write(10,*)'Shear Force(Y):',F_ss(2)
write(10,*)'Coefficient of Drag (Cd) :',(F_p(1)+F_ss(1))/(0.5*rho_inf*v_inf**2*chord)
write(10,*)'Coefficient of Lift (Cl) :',(F_p(2)+F_ss(2))/(0.5*rho_inf*v_inf**2*chord)
close(10)
print*,'Check Forces file'






end program idw_sb
