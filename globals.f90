module globals
implicit none
integer, parameter :: n_ib = 20000

!Number of intervals/cells
integer, parameter :: imax = 472 , jmax = 240 ,kmax = 1 ,n_vars = 14 

integer :: visc, fix, incl_band, st_size,iho
real :: xmu
integer,parameter :: n_IP = 2 
real :: rho_inf = 1.168
real :: v_inf = 278.
real :: P_inf = 101325.0
real :: chord = 1.
real :: xalp   !Angle of attack, taken as rotation of the freestream velocity vector
end module globals

