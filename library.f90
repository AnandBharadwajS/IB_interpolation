module library 
use globals
implicit none
contains

subroutine fix_stencil(R_ib,R_g,CI)
real, dimension(:,:) :: R_ib
real, dimension (:,:,:,:) :: R_g
integer, dimension (:,:) :: CI
real :: min_dist,dist, x_min, x_max, y_min, y_max, z_min, z_max
integer :: i_range, j_range, k_range, i, j, k, n
integer :: ist, jst, kst, iend, jend, kend
i_range = size(R_g,dim=1)
j_range = size(R_g,dim=2)
k_range = size(R_g,dim=3)
open(10,file='closest_indices.dat')
open(11,file='stencil_points.dat')
do n=1,n_ib
   if (n .eq. 1) then
      min_dist = 1.0e6
      do k=1,k_range
         do j=1,j_range
            do i=1,i_range
               dist = norm2(R_g(i,j,k,:)-R_ib(n,:))
               if(dist .lt. min_dist) then
                 min_dist = dist
                 CI(n,1) = i
                 CI(n,2) = j
                 CI(n,3) = k
               end if
            end do
         end do
      end do
      write(10,'(3i10)')CI(n,:)
      write(11,'(6e30.15)')R_ib(n,:),R_g(CI(n,1),CI(n,2),CI(n,3),:)
      goto 110
   end if
  
   ist  = max(CI(n-1,1)-1,1)
   iend = min(CI(n-1,1)+1,i_range)
   jst  = max(CI(n-1,2)-1,1)
   jend = min(CI(n-1,2)+1,j_range)
   kst  = max(CI(n-1,3)-1,1)
   kend = min(CI(n-1,3)+1,k_range)

   x_min = R_g(ist,jst,kst,1)
   x_max = R_g(iend,jend,kend,1)
   y_min = R_g(ist,jst,kst,2)
   y_max = R_g(iend,jend,kend,2)
   z_min = R_g(ist,jst,kst,3)
   z_max = R_g(iend,jend,kend,3)

   if(R_ib(n,1) .gt. x_min .and. R_ib(n,1) .lt. x_max .and. &
      R_ib(n,2) .gt. y_min .and. R_ib(n,2) .lt. y_max .and. &
      R_ib(n,3) .gt. z_min .and. R_ib(n,3) .lt. z_max)  then !The point lies inside the stencil of the previous closest cell-centre
      min_dist = 1.0e6
      do k=kst,kend
         do j=jst,jend
            do i=ist,iend
               dist = norm2(R_g(i,j,k,:)-R_ib(n,:))
               if(dist .lt. min_dist) then
                  min_dist = dist
                  CI(n,1) = i
                  CI(n,2) = j
                  CI(n,3) = k
               end if
            end do
         end do
      end do
   else  !The point does not lie inside the stencil of the previous stencil, a fresh search begins
      min_dist = 1.0e6
      do k=1,k_range
         do j=1,j_range
            do i=1,i_range
               dist = norm2(R_g(i,j,k,:)-R_ib(n,:))
               if(dist .lt. min_dist) then
                 min_dist = dist
                 CI(n,1) = i
                 CI(n,2) = j
                 CI(n,3) = k
               end if
            end do
         end do
      end do
   end if
   write(10,'(3i10)')CI(n,:)
   write(11,'(6e30.15)')R_ib(n,:),R_g(CI(n,1),CI(n,2),CI(n,3),:)
110 continue 
end do
close(10)
close(11)
end subroutine fix_stencil

subroutine idw_upwind(R_ib,R_g,CI,HH,phi,prop_in,prop_out,N,vel,T)
real, dimension(:,:) :: R_ib,N
real, dimension(:,:,:,:) :: R_g, vel
real, dimension(:,:,:) :: HH,phi, prop_in, T
real, dimension(:) :: prop_out
integer, dimension(:,:) :: CI
integer :: nn,i,j,k,w, ist,iend,jst,jend,kst,kend
integer :: i_range, j_range, k_range, st_okay
real :: d_i, vdotr, xmach, weight, sum_weight, gam, Rair, eps
real, dimension (3) :: vec_r, r_tau, v_tau
gam = 1.4
Rair = 287.
eps = 1.0e-10
prop_out(:) = 0.0
i_range = size(R_g,dim=1)
j_range = size(R_g,dim=2)
k_range = size(R_g,dim=3)
open(10,file='Pressure_IB.dat')
do nn=1,n_ib  !IB loop
   st_okay = 0
   w = st_size
   do while (st_okay .eq. 0)  !Loop to fix stencil size
      w = w+1
      ist  = max(1,CI(nn,1)-w)
      iend = min(i_range, CI(nn,1)+w)
      jst  = max(1,CI(nn,2)-w)
      jend = min(j_range,CI(nn,2)+w)
      kst  = max(1,CI(nn,3)-w)
      kend = min(k_range,CI(nn,3)+w)
      do k=kst,kend
         do j=jst,jend
            do i=ist,iend
               if(HH(i,j,k) .eq. 0.0) st_okay = 1
            end do
         end do
      end do

   end do  !Loop to fix stencil size. Final stencil size is stored in the variable 'w'
   
110  ist  = max(1,CI(nn,1)-w)
     iend = min(i_range, CI(nn,1)+w)
     jst  = max(1,CI(nn,2)-w)
     jend = min(j_range,CI(nn,2)+w)
     kst  = max(1,CI(nn,3)-w)
     kend = min(k_range,CI(nn,3)+w)

   sum_weight = 0.0
   prop_out(nn) = 0.0

   do k=kst,kend
      do j=jst,jend
         do i=ist,iend
            if ((n_vars .eq. 13 .and. HH(i,j,k).lt.0.5) .or. (n_vars .eq. 14 .and. phi(i,j,k).gt.0.0)) then
               d_i = norm2(R_ib(nn,:)-R_g(i,j,k,:))
               vec_r(:) = R_g(i,j,k,:)-R_ib(nn,:)
               r_tau(:) = vec_r(:) - (vec_r(1)*N(nn,1)+vec_r(2)*N(nn,2)+vec_r(3)*N(nn,3))*N(nn,:)
               v_tau(:) = vel(i,j,k,:) - (vel(i,j,k,1)*N(nn,1)+vel(i,j,k,2)*N(nn,2)+vel(i,j,k,3)*N(nn,3))*N(nn,:)
               vdotr = vel(i,j,k,1)*r_tau(1)+vel(i,j,k,2)*r_tau(2)+vel(i,j,k,3)*r_tau(3)
               xmach = norm2(v_tau)/sqrt(gam*Rair*T(i,j,k))
               if (xmach .lt. 1.0) weight = 0.25*(1-xmach*sign(1.0,vdotr))**2*(2.0+xmach*sign(1.0,vdotr))/(d_i+eps)
               if (xmach .ge. 1.0) weight = 1.0/d_i*(1.0-sign(1.0,vdotr))/2.0
               prop_out(nn) = prop_out(nn) + prop_in(i,j,k)*weight
               sum_weight = sum_weight + weight
            end if
         end do
      end do
   end do
   
   if(sum_weight .eq. 0.0) then
     print*,'Sum of weights is zero. Increasing stencil size'
     w=w+1
     goto 110
   end if

   prop_out(nn) = prop_out(nn)/sum_weight
   write(10,'(2e30.15)')R_ib(nn,1),prop_out(nn)
end do  !IB loop ends
close(10)

end subroutine idw_upwind

subroutine idw_ip(R_ib,R_g,CI,HH,phi,prop_in,prop_out,d_ip,N)
real, dimension(:,:) :: R_ib,N
real, dimension(:,:,:,:) :: R_g
real, dimension(:,:,:) :: HH,phi
real, dimension(:,:,:,:) :: prop_in
real, dimension(:,:) :: d_ip
real, dimension(:,:,:) :: prop_out !index 1- IB point number, index 2- property, index 3- IP point
integer, dimension(:,:) :: CI
integer :: i,j,k,i_range, j_range, k_range,nn, st_okay, w, ip
integer :: ist,iend,jst,jend,kst,kend, cii, cij, cik
real :: gam, Rair,eps, rdotn, d_i, theta, d_para, d_perp, weight, sum_weight, dist_min, xdist
real, dimension(3) :: vec_r, vec_IP

gam = 1.4
Rair = 287.
eps = 1.0e-10
i_range = size(R_g,dim=1)
j_range = size(R_g,dim=2)
k_range = size(R_g,dim=3)
open(10,file='v_IP.dat')
do nn=1,n_ib  !IB loop
   do ip = 1,n_IP !Interpolation point loop 
       if (ip .eq. 1) then
          w = st_size
          st_okay = 0
       else if(ip .eq. 2 .and. iho .eq. 1) then
          w=w+1  !Increasing the stencil size for the second IP (SCDS)
       end if
       do while (st_okay .eq. 0) !To fix stencil size
            w = w+1
            ist  = max(1,CI(nn,1)-w)
            iend = min(i_range, CI(nn,1)+w)
            jst  = max(1,CI(nn,2)-w)
            jend = min(j_range,CI(nn,2)+w)
            kst  = max(1,CI(nn,3)-w)
            kend = min(k_range,CI(nn,3)+w)

            do k=kst,kend
               do j=jst,jend
                  do i=ist,iend
                     if(HH(i,j,k) .eq. 0.0) st_okay = 1
                  end do
               end do
            end do
       end do  !Stencil fix loop ends

       if (iho .eq. 2 .and. ip .eq. 2) then  !Algorithm for DCSS (Different Centre Same stencil Size)
          vec_IP(:) = R_ib(nn,:) + d_ip(nn,1)*N(nn,:)
          dist_min = 1.0e6

          ist  = max(1,CI(nn,1)-w)
          iend = min(i_range, CI(nn,1)+w)
          jst  = max(1,CI(nn,2)-w)
          jend = min(j_range,CI(nn,2)+w)
          kst  = max(1,CI(nn,3)-w)
          kend = min(k_range,CI(nn,3)+w)

          !Finding the centre of the second stencil
          do k=kst,kend
             do j=jst,jend
                do i=ist,iend
                   xdist = norm2(vec_IP(:)-R_g(i,j,k,:))
                   if(xdist .lt. dist_min) then
                     dist_min = xdist
                     cii = i
                     cij = j
                     cik = k
                   end if
                end do
             end do
          end do
          
          !Condition for the centre of second stencil centre coinciding with that of the first
          if((cii .eq. CI(nn,1)) .and. (cij .eq. CI(nn,2)) .and. (cik .eq. CI(nn,3))) w = w+1  
    
       else
          cii = CI(nn,1)
          cij = CI(nn,2)
          cik = CI(nn,3)
       end if


       ist  = max(1,cii-w)
       iend = min(i_range, cii+w)
       jst  = max(1,cij-w)
       jend = min(j_range,cij+w)
       kst  = max(1,cik-w)
       kend = min(k_range,cik+w)
       
       sum_weight = 0.0 
       prop_out(nn,:,ip) = 0.0
       d_ip(nn,ip) = 0.0
       do k=kst,kend
          do j=jst,jend
             do i=ist,iend
                if ((n_vars .eq. 13 .and. HH(i,j,k).lt.0.5) .or. (n_vars .eq. 14 .and. phi(i,j,k).gt.0.0)) then
                  d_i = norm2(R_ib(nn,:)-R_g(i,j,k,:))
                  vec_r(:) = R_g(i,j,k,:)-R_ib(nn,:)
                  rdotn = vec_r(1)*N(nn,1)+vec_r(2)*N(nn,2)+vec_r(3)*N(nn,3)
                  theta = acos(rdotn/(d_i+eps))
                  d_para = rdotn
                  d_perp = sqrt(d_i**2-d_para**2)
                  if(cos(theta).gt.0.0) then  !Ruling out cells that make obtuse angles with the normal
                    weight = 1.0/(d_perp+eps)
                    prop_out(nn,:,ip) = prop_out(nn,:,ip) + prop_in(i,j,k,:)*weight
                    d_ip(nn,ip) = d_ip(nn,ip) + d_para*weight
                    sum_weight = sum_weight + weight
                  end if
               end if
             end do
          end do
       end do
       if(sum_weight .eq. 0.0) then
         print*,'sum of the weights is zero. Please inspect. IB point:',nn
         stop
       end if  
       prop_out(nn,:,ip) = prop_out(nn,:,ip)/sum_weight
       d_ip(nn,ip) = d_ip(nn,ip)/sum_weight

       write(10,'(4e30.15)')R_ib(nn,1:2)+d_ip(nn,ip)*N(nn,1:2),prop_out(nn,:,ip)

    end do!End of loop of interpolation points

end do !IB loop ends
close(10)

end subroutine idw_ip

subroutine shear_stress(R_ib,R_g,CI,V_ip,d_ip,N,Tau)
real, dimension(:,:) :: R_ib,N
real, dimension(:,:,:,:) :: R_g
real, dimension(:,:) :: Tau, d_ip
real, dimension (:,:,:) :: V_ip
integer, dimension(:,:) :: CI
integer :: i,j,k,i_range, j_range, k_range,nn, st_okay, w
real, dimension (3) :: v_tau1, v_tau2
real :: xmod, dUdY, a_n

i_range = size(R_g,dim=1)
j_range = size(R_g,dim=2)
k_range = size(R_g,dim=3)
open(11,file='Tau.dat')
write(11,*)'Variables = X,Y,tx,ty,tz,Tau'
do nn=1,n_ib !IB loop
   if(n_IP .eq. 1) then
     v_tau1 = V_ip(nn,:,n_IP) - (V_ip(nn,1,n_IP)*N(nn,1) + &
				V_ip(nn,2,n_IP)*N(nn,2) + &
                                V_ip(nn,3,n_IP)*N(nn,3))*N(nn,1:3)
     xmod = norm2(v_tau1)
     Tau(nn,1:3) = v_tau1(1:3)/xmod  !Direction of the tangent vector
     Tau(nn,4) = xmu*(1-visc)*xmod/d_ip(nn,n_IP)
     write(11,'(6e30.15)')R_ib(nn,1:2),Tau(nn,1:3),Tau(nn,4)
   end if !End of condition for 1 IP

   if(n_IP .eq. 2) then !Condition for 2 IPs
    v_tau1 = V_ip(nn,:,1) - (V_ip(nn,1,1)*N(nn,1) + &
			     V_ip(nn,2,1)*N(nn,2) + &
                             V_ip(nn,3,1)*N(nn,3))*N(nn,1:3)
    
    v_tau2 = V_ip(nn,:,2) - (V_ip(nn,1,2)*N(nn,1) + &
			     V_ip(nn,2,2)*N(nn,2) + &
                             V_ip(nn,3,2)*N(nn,3))*N(nn,1:3)
    a_n = (norm2(v_tau1)*d_ip(nn,2)-norm2(v_tau2)*d_ip(nn,1))/(d_ip(nn,1)*d_ip(nn,2)*(d_ip(nn,1)-d_ip(nn,2)))
    if(a_n .gt. 0.0 .and. fix .eq. 1) then !In case of incorrect prediction of profile
      dUdY = norm2(v_tau1)/d_ip(nn,1) 
    else 
      dUdY = (norm2(v_tau1)*d_ip(nn,2)**2-norm2(v_tau2)*d_ip(nn,1)**2)/(d_ip(nn,1)*d_ip(nn,2)*(d_ip(nn,2)-d_ip(nn,1)))
    end if
    xmod = norm2(v_tau1)
    Tau(nn,1:3) = v_tau1(1:3)/xmod  !Direction of the tangent vector
    Tau(nn,4) = xmu*dUdY
    write(11,'(6e30.15)')R_ib(nn,1:2),Tau(nn,1:3),Tau(nn,4)
  
   end if !End of condition for 2 IPs
end do !IB loop ends

close(11)
end subroutine shear_stress

!Boundary Layer Edge Detection Method
subroutine BLEDM(R_ib,N,R_g,CI,vel,Tau,d_ip,V_ip,phi) 
real, dimension(:,:) :: R_ib,N
real, dimension(:,:,:,:) :: R_g, vel
real, dimension(:,:) :: Tau, d_ip
real, dimension (:,:,:) :: V_ip,phi
real, dimension(4) :: xdist
real, dimension(3) :: vec_r
real, dimension(2) :: v1_ip, bl, vt_ip
real :: omega, omega_o, dist, dist_min, d_i, d_para, d_perp, d1_ip
real :: weight, sum_weight, eps, xnorm, xa, xb
integer, dimension(:,:) :: CI
integer :: ist, iend, jst, jend, kst, kend
integer :: i,j,k,i_range, j_range, k_range,nn, st_okay, w, st_count, sci, scj, sck
integer :: ind
eps = 1.0e-10
i_range = size(R_g,dim=1)
j_range = size(R_g,dim=2)
k_range = size(R_g,dim=3)
open(15,file='BL_edge.dat')
open(11,file='Tau.dat')
write(11,*)'Variables = X,Y,tx,ty,tz,Tau'

!!----3----!
!!         !
!4         2
!! 	   !	
!!----1----!
do nn=1,n_ib  !IB loop starts
   omega = 1.0
   omega_o = 1.0
   st_count = 1
   w = 1
   sci = CI(nn,1) !stencil centre - i
   scj = CI(nn,2) !stencil centre - j
   sck = CI(nn,3) !stencil centre - k

   !Finding velocity of first interpolation point
   ist  = max(1,sci-w)
   iend = min(i_range, sci+w)
   jst  = max(1,scj-w)
   jend = min(j_range,scj+w)
   kst  = max(1,sck-w)
   kend = min(k_range,sck+w)
   v1_ip(:) = 0.0
   d1_ip = 0.0 
   sum_weight = 0.0
   do k=kst,kend
      do j=jst,jend
         do i=ist,iend
            vec_r = R_g(i,j,k,1:3)-R_ib(nn,1:3) 
            d_para = vec_r(1)*N(nn,1) + vec_r(2)*N(nn,2)
            d_i = norm2(vec_r)
            d_perp = sqrt(d_i**2-d_para**2)
            weight = 1.0/(d_perp+eps)*0.5*(1.0+sign(1.0,phi(i,j,k)))
            weight = weight*0.5*(1.0+sign(1.0,d_para)) !To rule out cells that subtend obtuse angle with the surface normal
            v1_ip = v1_ip + vel(i,j,k,1:2)*weight
            d1_ip = d1_ip + d_para*weight
            sum_weight = sum_weight + weight
         end do
      end do
   end do
   v1_ip = v1_ip / sum_weight
   d1_ip = d1_ip / sum_weight
   
   if(v1_ip(1)*v_inf*cos(xalp) + v1_ip(2)*v_inf*sin(xalp) .lt. 0.0) then !Condition for separation
     vt_ip = v1_ip - (v1_ip(1)*N(nn,1)+v1_ip(2)*N(nn,2))*N(nn,1:2)
     xnorm = norm2(vt_ip)
     Tau(nn,1:2) = vt_ip/xnorm
     Tau(nn,3) = 0.0
     Tau(nn,4) = xmu*xnorm/d1_ip
     bl(1:2) = R_ib(nn,1:2) + d1_ip*N(nn,1:2)

   else
 
     
     do while (abs(omega/omega_o) .gt. 0.05) 
     !do while (st_count .lt. 20) 
        ist  = max(1,sci-w)
        iend = min(i_range, sci+w)
        jst  = max(1,scj-w)
        jend = min(j_range,scj+w)
        kst  = max(1,sck-w)
        kend = min(k_range,sck+w)
        
        if(st_count .ne. 1) then  !If not first stencil
          omega = (vel(iend,scj,1,2)-vel(ist,scj,1,2))/(R_g(iend,scj,1,1)-R_g(ist,scj,1,1)) - &
                  (vel(sci,jend,1,1)-vel(sci,jst,1,1))/(R_g(sci,jend,1,2)-R_g(sci,jst,1,2))
          if(st_count .eq. 2) omega_o = omega
        end if

        xdist(:) = 0.0
        do k = kst,kend
           do j = jst,jend
              do i = ist, iend
                 vec_r = R_g(i,j,k,1:3)-R_ib(nn,1:3)
                 dist = vec_r(1)*N(nn,1) + vec_r(2)*N(nn,2) 
                 if(j.eq.jst)  xdist(1) = xdist(1) + dist  !Face-1 (Refer figure before loop start)
                 if(i.eq.iend) xdist(2) = xdist(2) + dist  !Face-2
                 if(j.eq.jend) xdist(3) = xdist(3) + dist  !Face-3
                 if(i.eq.ist)  xdist(4) = xdist(4) + dist  !Face-4
              end do
           end do
        end do
        xdist(1) = xdist(1) / (iend-ist+1)
        xdist(2) = xdist(2) / (jend-jst+1)
        xdist(3) = xdist(3) / (iend-ist+1)
        xdist(4) = xdist(4) / (jend-jst+1)
        ind = maxloc(xdist,dim=1)
        
        if (ind .eq. 1) then
           j = jst
           dist_min = 1.0e6
           do i=ist,iend
              vec_r =  R_g(i,j,1,1:3) - R_ib(nn,1:3)
              d_i = norm2(vec_r)
              d_para = vec_r(1)*N(nn,1)+vec_r(2)*N(nn,2)+vec_r(3)*N(nn,3)
              dist = sqrt(d_i**2-d_para**2)
              if(dist .lt. dist_min) then
                dist_min = dist
                sci = i
                scj = j
                sck = 1
              end if  
           end do
        end if
        if (ind .eq. 2) then
           i=iend
           dist_min = 1.0e6
           do j=jst,jend
              vec_r =  R_g(i,j,1,1:3) - R_ib(nn,1:3)
              d_i = norm2(vec_r)
              d_para = vec_r(1)*N(nn,1)+vec_r(2)*N(nn,2)+vec_r(3)*N(nn,3)
              dist = sqrt(d_i**2-d_para**2)
              if(dist .lt. dist_min) then
                dist_min = dist
                sci = i
                scj = j
                sck = 1
              end if  
           end do
        end if
        if (ind .eq. 3) then
           j=jend
           dist_min = 1.0e6
           do i=ist,iend
              vec_r =  R_g(i,j,1,1:3) - R_ib(nn,1:3)
              d_i = norm2(vec_r)
              d_para = vec_r(1)*N(nn,1)+vec_r(2)*N(nn,2)+vec_r(3)*N(nn,3)
              dist = sqrt(d_i**2-d_para**2)
              if(dist .lt. dist_min) then
                dist_min = dist
                sci = i
                scj = j
                sck = 1
              end if  
           end do
        end if
        if (ind .eq. 4) then
           i=ist
           dist_min = 1.0e6
           do j=jst,jend
              vec_r =  R_g(i,j,1,1:3) - R_ib(nn,1:3)
              d_i = norm2(vec_r)
              d_para = vec_r(1)*N(nn,1)+vec_r(2)*N(nn,2)+vec_r(3)*N(nn,3)
              dist = sqrt(d_i**2-d_para**2)
              if(dist .lt. dist_min) then
                dist_min = dist
                sci = i
                scj = j
                sck = 1
              end if  
           end do
        end if
        st_count = st_count + 1
     end do !Vorticity-check loop
     !print*,omega, omega_o
     !Finding the interpolation point of the BL-edge stencil
     ist  = max(1,sci-w)
     iend = min(i_range, sci+w)
     jst  = max(1,scj-w)
     jend = min(j_range,scj+w)
     kst  = max(1,sck-w)
     kend = min(k_range,sck+w)
     v1_ip(:) = 0.0
     d1_ip = 0.0 
     sum_weight = 0.0
     do k=kst,kend
        do j=jst,jend
           do i=ist,iend
              vec_r = R_g(i,j,k,1:3)-R_ib(nn,1:3) 
              d_para = vec_r(1)*N(nn,1) + vec_r(2)*N(nn,2)
              d_i = norm2(vec_r)
              d_perp = sqrt(d_i**2-d_para**2)
              weight = 1.0/(d_perp+eps)*0.5*(1.0+sign(1.0,phi(i,j,k)))
              weight = weight*0.5*(1.0+sign(1.0,d_para)) !To rule out cells that subtend obtuse angle with the surface normal
              v1_ip = v1_ip + vel(i,j,k,1:2)*weight
              d1_ip = d1_ip + d_para*weight
              sum_weight = sum_weight + weight
           end do
        end do
     end do
     v1_ip = v1_ip / sum_weight
     d1_ip = d1_ip / sum_weight
     vt_ip = v1_ip - (v1_ip(1)*N(nn,1)+v1_ip(2)*N(nn,2))*N(nn,1:2)
     xnorm = norm2(vt_ip)
     Tau(nn,1:2) = vt_ip/xnorm
     Tau(nn,3) = 0.0
     xa = -norm2(vt_ip)/d1_ip**2
     xb = -2.0*xa*d1_ip
     Tau(nn,4) = xmu*xb
     bl(1:2) = R_ib(nn,1:2) + d1_ip*N(nn,1:2)

   end if !End of condition for separation
   !Writing out the BL-edge points
   write(15,'(3e30.15)')bl(1:2)
   write(11,'(6e30.15)')bl(1:2),Tau(nn,1:3)/1000.0,Tau(nn,4)
end do  !IB loop ends
close(15)
close(11)
end subroutine BLEDM 

end module library

