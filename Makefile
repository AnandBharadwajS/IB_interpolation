FLAGS =-traceback -r8 -O2
main:
	@ifort $(FLAGS) -c globals.f90
	@ifort $(FLAGS) -c library.f90
	@ifort $(FLAGS) globals.o library.o Interpolation.f90 -o interpolate
